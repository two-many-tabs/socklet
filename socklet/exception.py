class SockletException(Exception):
    pass


class SockletArgumentException(SockletException):
    pass


class SockletConfigurationException(SockletException):
    pass
